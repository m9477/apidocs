"use strict";

const express = require("express");
const app = express();

const bodyParser = require("body-parser");
const { port } = require("..");

const jsonParser = bodyParser.json();

/**
 * @swagger
 *  components:
 *      schemas:
 *          Ride:
 *              type: Object
 *              properties:
 *
 *                  startLatitude:
 *                                  type: integer
 *                  startLongitude:
 *                                  type: integer
 *                  endLatitude:
 *                                  type: integer
 *                  endLongitude:
 *                                  type: integer
 *                  riderName:
 *                                  type: string
 *                  driverName:
 *                                  type: string
 *                  driverVehicle:
 *                                  type: string
 */

module.exports = (db) => {
  /**
   * @swagger
   * /health:
   *  get:
   *    description: Use to get the health of the API
   *    responses:
   *      '200':
   *        description: Healthy
   */

  app.get("/health", (req, res) => res.send("Healthy"));

  /**
   * @swagger
   * /rides:
   *  post:
   *    description: Use to add rides to the API
   *    requestBody:
   *        description: user info to be filled
   *        required: true
   *        content:
   *            application/json:
   *                schema:
   *                    type: object
   *                    required:
   *                        - username
   *                        - name
   *                    properties:
   *                        username:
   *                            type: string
   *                            description: string
   *    responses:
   *      '200':
   *        description: Latest Ride
   */

  app.post("/rides", jsonParser, (req, res) => {
    const startLatitude = Number(req.body.start_lat);
    const startLongitude = Number(req.body.start_long);
    const endLatitude = Number(req.body.end_lat);
    const endLongitude = Number(req.body.end_long);
    const riderName = req.body.rider_name;
    const driverName = req.body.driver_name;
    const driverVehicle = req.body.driver_vehicle;

    if (
      startLatitude < -90 ||
      startLatitude > 90 ||
      startLongitude < -180 ||
      startLongitude > 180
    ) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message:
          "Start latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
      });
    }

    if (
      endLatitude < -90 ||
      endLatitude > 90 ||
      endLongitude < -180 ||
      endLongitude > 180
    ) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message:
          "End latitude and longitude must be between -90 - 90 and -180 to 180 degrees respectively",
      });
    }

    if (typeof riderName !== "string" || riderName.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    if (typeof driverName !== "string" || driverName.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    if (typeof driverVehicle !== "string" || driverVehicle.length < 1) {
      return res.send({
        error_code: "VALIDATION_ERROR",
        message: "Rider name must be a non empty string",
      });
    }

    var values = [
      req.body.start_lat,
      req.body.start_long,
      req.body.end_lat,
      req.body.end_long,
      req.body.rider_name,
      req.body.driver_name,
      req.body.driver_vehicle,
    ];

    const result = db.run(
      "INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES (?, ?, ?, ?, ?, ?, ?)",
      values,
      function (err) {
        if (err) {
          return res.send({
            error_code: "SERVER_ERROR",
            message: "Unknown error",
          });
        }

        db.all(
          "SELECT * FROM Rides WHERE rideID = ?",
          this.lastID,
          function (err, rows) {
            if (err) {
              return res.send({
                error_code: "SERVER_ERROR",
                message: "Unknown error",
              });
            }

            res.send(rows);
          }
        );
      }
    );
  });

  /**
   * @swagger
   * /rides:
   *  get:
   *    description: Use to get all rides from the API
   *
   *    responses:
   *      '200':
   *        description: Rides List
   */

  app.get("/rides", (req, res) => {
    db.all("SELECT * FROM Rides", function (err, rows) {
      if (err) {
        return res.send({
          error_code: "SERVER_ERROR",
          message: "Unknown error",
        });
      }

      if (rows.length === 0) {
        return res.send({
          error_code: "RIDES_NOT_FOUND_ERROR",
          message: "Could not find any rides",
        });
      }

      res.send(rows);
    });
  });

  /**
   * @swagger
   * /rides/{id}:
   *  get:
   *    description: Use to get a single ride from the API
   *
   *    responses:
   *      '200':
   *        description: A Single Ride
   */

  app.get("/rides/:id", (req, res) => {
    db.all(
      `SELECT * FROM Rides WHERE rideID='${req.params.id}'`,
      function (err, rows) {
        if (err) {
          return res.send({
            error_code: "SERVER_ERROR",
            message: "Unknown error",
          });
        }

        if (rows.length === 0) {
          return res.send({
            error_code: "RIDES_NOT_FOUND_ERROR",
            message: "Could not find any rides",
          });
        }

        res.send(rows);
      }
    );
  });

  return app;
};
