"use strict";

const express = require("express");
const app = express();
const port = 8010;

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(":memory:");

const swaggerOptions = {
  swaggerDefinition: {
    components: {},
    info: {
      version: "1.0.0",
      title: "Rides API",
      description: "Rides API endpoints",
      contact: {
        name: "Amazing Developer",
      },
      servers: [`http://localhost:8010`],
    },
  },
  // ['.routes/*.js']
  apis: [`${__dirname}/src/app.js`],
};

const buildSchemas = require("./src/schemas");

db.serialize(() => {
  const app = require("./src/app")(db);
  buildSchemas(db);
  const swaggerDocs = swaggerJSDoc(swaggerOptions);
  app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

  app.listen(port, () =>
    console.log(`App started and listening on port ${port}`)
  );
});
